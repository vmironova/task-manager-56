package ru.t1consulting.vmironova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.vmironova.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show application commands and arguments.";

    @NotNull
    public static final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        commands.forEach(System.out::println);
    }

}
