package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.repository.model.IRepository;
import ru.t1consulting.vmironova.tm.enumerated.Sort;
import ru.t1consulting.vmironova.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

}
