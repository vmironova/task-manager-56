package ru.t1consulting.vmironova.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {
}
